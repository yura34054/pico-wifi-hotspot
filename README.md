# Meta patched this out but it likely still works on Pico headsets and other similar Android based units.

# Quest-WiFiHotspot

## Installation
- Download the APK
- Enable developer mode on the headset if not already
- Install via SideQuest
- Go to Search -> Unknown Sources and find the app

## Support
My name's tenten8401 on discord, feel free to DM me or I'm also in the Virtual Desktop / VRChat discord servers.

## Project status
If anything's broken make an issue report, but due to the simple nature of this it probably won't get many or any updates.

## License
Do whatever, I don't care, it's Unlicense :p
